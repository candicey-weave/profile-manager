package com.gitlab.candicey.profilemanager

import kotlinx.serialization.json.Json
import java.io.File

val json = Json {
    ignoreUnknownKeys = true
}

val configDirectory: File =
    File(System.getProperty("user.home") + "/.config/profile-manager")
        .apply { mkdirs() }

val profilesDirectory: File =
    File(configDirectory, "profiles")
        .apply { mkdirs() }

val profilesConfig: File =
    File(configDirectory, "profiles.json")
        .apply { createNewFile() }