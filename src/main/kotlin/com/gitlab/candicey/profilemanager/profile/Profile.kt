package com.gitlab.candicey.profilemanager.profile

import com.gitlab.candicey.profilemanager.profilesDirectory
import com.gitlab.candicey.profilemanager.sha256
import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient
import java.io.File
import kotlin.io.path.Path

@Serializable
data class Profile(
    val pathString: String,
    val options: MutableList<FileOption> = mutableListOf(),
) {
    val path by lazy { Path(pathString) }

    val directory by lazy { File(profilesDirectory, pathString.sha256) }

    init {
        refresh()
    }

    fun refresh() {
        options.forEach { it.parentProfile = this }
    }

    fun createFileOptions(vararg options: File, add: Boolean = true): List<FileOption> =
        options
            .map { FileOption(it.name) }
            .onEach { it.parentProfile = this }
            .filterNot { fileOption ->
                fileOption.file.exists().also {
                    if (it) {
                        println("File ${fileOption.file.canonicalPath} already exists, skipping...")
                    }
                }
            }
            .also { if (add) this.options.addAll(it) }
}

@Serializable
data class FileOption(
    val name: String,
) : Comparable<FileOption> {
    @Transient
    var parentProfile: Profile? = null

    val file by lazy { File(parentProfile!!.directory, name) }

    override fun compareTo(other: FileOption): Int = name.compareTo(other.name)
}