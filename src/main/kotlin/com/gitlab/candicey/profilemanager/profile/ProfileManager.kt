package com.gitlab.candicey.profilemanager.profile

import com.gitlab.candicey.profilemanager.configDirectory
import com.gitlab.candicey.profilemanager.isSameAs
import com.gitlab.candicey.profilemanager.json
import com.gitlab.candicey.profilemanager.profilesConfig
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import java.io.File
import java.nio.file.Path

@Suppress("MemberVisibilityCanBePrivate")
object ProfileManager {
    private val profiles: MutableList<Profile> = run {
        val text = profilesConfig.readText()

        runCatching {
            json.decodeFromString<MutableList<Profile>>(text)
        }.getOrElse {
            if (text.isNotBlank()) {
                val corruptedBackup = File(configDirectory, "profiles-${System.currentTimeMillis()}.json.corrupted")
                profilesConfig.copyTo(corruptedBackup)
                println("Corrupted config file, backup created at ${corruptedBackup.canonicalPath}")
            }
            profilesConfig.writeText("[]")
            mutableListOf()
        }
    }

    fun exists(profile: Profile): Boolean =
        profiles.contains(profile)

    fun exists(path: Path): Boolean =
        profiles.any { it.path isSameAs path }

    fun add(profile: Profile): Boolean = saveContext {
        if (exists(profile) || exists(profile.path)) {
            save = false
            return@saveContext false
        }

        profiles.add(profile)
        return@saveContext true
    }

    fun forceAdd(profile: Profile) = saveContext {
        profiles.removeIf { it.path isSameAs profile.path }
        profiles.add(profile)
    }

    fun remove(profile: Profile): Boolean = saveContext {
        if (!exists(profile)) {
            save = false
            return@saveContext false
        }

        profiles.remove(profile)
        return@saveContext true
    }

    fun remove(path: Path): Boolean = saveContext {
        if (!exists(path)) {
            save = false
            return@saveContext false
        }

        profiles.removeIf { it.path isSameAs path }
        return@saveContext true
    }

    fun clear() = saveContext {
        profiles.clear()
    }

    fun get(path: Path): Profile? =
        profiles.firstOrNull { it.path isSameAs path }

    fun getAll(): List<Profile> = profiles.toList()

    fun save() = profilesConfig.writeText(json.encodeToString(profiles))

    class SaveContext {
        var save = true
    }

    fun <T> saveContext(block: SaveContext.() -> T): T {
        val context = SaveContext()
        val result = context.block()
        if (context.save) {
            save()
        }
        return result
    }
}