package com.gitlab.candicey.profilemanager.helper

import com.gitlab.candicey.profilemanager.runProcess
import java.io.File

object SymbolicLinkHelper {
    private fun ln(vararg args: String) = runProcess("ln", *args)

    fun createSymbolicLink(target: String, link: String, soft: Boolean = true) =
        if (soft) {
            ln("-s", target, link)
        } else {
            ln(target, link)
        }

    fun createSymbolicLink(target: File, link: File, soft: Boolean = true) =
        createSymbolicLink(target.canonicalPath, link.canonicalPath, soft)
}