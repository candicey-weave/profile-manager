package com.gitlab.candicey.profilemanager

import com.gitlab.candicey.profilemanager.mode.ModeManager
import com.gitlab.candicey.profilemanager.profile.ProfileManager
import java.io.File
import java.nio.file.FileSystems
import java.nio.file.Files
import kotlin.io.path.Path

fun main(args: Array<String>) {
    if (args.isEmpty()) {
        println("Usage: java -jar ${ProfileManager.javaClass.jarPath?.let { FileSystems.getDefault().getPath(System.getProperty("user.dir")).relativize(it) } ?: "ProfileManager.jar"} <mode> [args]")
        return
    }

    val mode = ModeManager.getMode(args[0])
    if (mode == null) {
        println("Unknown mode: ${args[0]}")
        println("Available modes: ${ModeManager.getAllModes().joinToString(", ") { it.names[0] }}")
        return
    }

    mode.run(args.drop(1).toList())
}