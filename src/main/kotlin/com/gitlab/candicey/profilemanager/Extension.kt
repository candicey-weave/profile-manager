package com.gitlab.candicey.profilemanager

import com.gitlab.candicey.profilemanager.profile.Profile
import com.gitlab.candicey.profilemanager.profile.ProfileManager
import java.io.File
import java.nio.file.Files
import java.nio.file.Path
import java.security.MessageDigest

infix fun File.isLinked(other: File): Boolean =
    canonicalFile isSameAs other.canonicalFile

infix fun File.isSameAs(other: File): Boolean =
    Files.isSameFile(toPath(), other.toPath())

infix fun Path.isSameAs(other: Path): Boolean =
    Files.isSameFile(this, other)

fun Path.toProfile(): Profile? =
    ProfileManager.get(this)

fun Path.toProfileOrCreate(add: Boolean = true): Profile =
    toProfile()
        ?: Profile(this.toFile().canonicalPath)
            .also {
                if (add) {
                    ProfileManager.add(it)
                }
            }

/**
 * Create a profile from a path, if the profile already exists, return the profile and false, otherwise create a new profile and return the profile and true
 *
 * @return Pair<profile, isNew>
 */
fun Path.toProfileOrCreateVerbose(add: Boolean = true): Pair<Profile, Boolean> {
    val profile = toProfile()
    if (profile != null) {
        return profile to false
    }

    val newProfile = Profile(this.toFile().canonicalPath)
    if (add) {
        ProfileManager.add(newProfile)
    }

    return newProfile to true
}

val Class<*>.jarPath: Path?
    get() = protectionDomain?.codeSource?.location?.toURI()?.let(Path::of)

val String.sha256: String
    get() = MessageDigest.getInstance("SHA-256")
        .digest(this.toByteArray())
        .joinToString("") { "%02x".format(it) }