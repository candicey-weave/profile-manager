package com.gitlab.candicey.profilemanager

fun runProcess(executable: String, vararg args: String): Process =
    ProcessBuilder(executable, *args).start()