package com.gitlab.candicey.profilemanager.mode.impl

import com.gitlab.candicey.profilemanager.helper.SymbolicLinkHelper
import com.gitlab.candicey.profilemanager.mode.IMode
import com.gitlab.candicey.profilemanager.mode.findTargetFile
import com.gitlab.candicey.profilemanager.toProfileOrCreateVerbose

object SwitchProfile : IMode {
    override val names: List<String>
        get() = listOf("switch", "sw")

    override fun run(args: List<String>) {
        if (args.size < 2) {
            println("Please specify a profile and an option to switch to.")
            return
        }

        val targetFile = findTargetFile(args) ?: return

        val (profile, new) = targetFile.toPath().toProfileOrCreateVerbose(add = false)

        if (new) {
            println("Profile does not exist!")
            return
        }

        val option = profile.options.firstOrNull { it.name == args[1] }
        if (option == null) {
            println("Option does not exist!")
            return
        }

        targetFile.delete()
        SymbolicLinkHelper.createSymbolicLink(option.file, targetFile)
        println("Switched to ${option.name}!")
    }
}