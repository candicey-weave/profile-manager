package com.gitlab.candicey.profilemanager.mode

import java.io.File

fun findTargetFile(args: List<String>, checkExists: Boolean = true): File? {
    if (args.isEmpty()) {
        println("Please specify a file to add.")
        return null
    }

    val file = File(args[0])
    if (checkExists && !file.exists()) {
        println("File does not exist: ${file.absolutePath}")
        return null
    }

    return file
}

fun confirm(prompt: String): Boolean {
    print("$prompt [y/N] ")
    val input = readlnOrNull() ?: return false
    return input.toLowerCase() == "y"
}