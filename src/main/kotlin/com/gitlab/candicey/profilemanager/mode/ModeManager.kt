package com.gitlab.candicey.profilemanager.mode

import com.gitlab.candicey.profilemanager.mode.impl.*

object ModeManager {
    private val modes = listOf(
        AddProfile,
        ListProfile,
        RemoveOption,
        SwitchProfile,
        GetHash,
        SearchHash,
    )

    fun getMode(name: String): IMode? = modes.find { mode ->
        mode.names.any {
            it.equals(name, ignoreCase = true)
        }
    }

    fun getAllModes(): List<IMode> = modes.toList()
}