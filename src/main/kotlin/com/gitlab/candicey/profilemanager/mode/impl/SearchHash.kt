package com.gitlab.candicey.profilemanager.mode.impl

import com.gitlab.candicey.profilemanager.mode.IMode
import com.gitlab.candicey.profilemanager.profile.ProfileManager
import com.gitlab.candicey.profilemanager.sha256

object SearchHash : IMode {
    override val names: List<String>
        get() = listOf("searchhash", "sh")

    override fun run(args: List<String>) {
        if (args.isEmpty()) {
            println("Please provide a hash to search for!")
            return
        }

        val hash = args[0]

        val profile = ProfileManager.getAll().find { it.pathString.sha256 == hash }
        if (profile == null) {
            println("No profiles found with hash $hash!")
            return
        }

        println("Found profile with hash $hash: ${profile.pathString}")
    }
}