package com.gitlab.candicey.profilemanager.mode.impl

import com.gitlab.candicey.profilemanager.isLinked
import com.gitlab.candicey.profilemanager.mode.IMode
import com.gitlab.candicey.profilemanager.mode.findTargetFile
import com.gitlab.candicey.profilemanager.toProfileOrCreateVerbose

object ListProfile : IMode {
    override val names: List<String>
        get() = listOf("list", "ls", "l")

    override fun run(args: List<String>) {
        val targetFile = findTargetFile(args) ?: return

        val (profile, new) = targetFile.toPath().toProfileOrCreateVerbose(add = false)

        if (new) {
            println("Profile does not exist!")
            return
        }

        profile.options.sort()

        println("Profile: ${profile.pathString} with ${profile.options.size} option(s).")
        profile.options.forEach { println(" ${if (runCatching { it.file isLinked targetFile }.getOrElse { false }) "*" else "-"} ${it.name}") }
    }
}