package com.gitlab.candicey.profilemanager.mode.impl

import com.gitlab.candicey.profilemanager.mode.IMode
import com.gitlab.candicey.profilemanager.mode.findTargetFile
import com.gitlab.candicey.profilemanager.sha256
import com.gitlab.candicey.profilemanager.toProfileOrCreateVerbose

object GetHash : IMode {
    override val names: List<String>
        get() = listOf("gethash", "hash", "gh")

    override fun run(args: List<String>) {
        val targetFile = findTargetFile(args) ?: return

        val (profile, new) = targetFile.toPath().toProfileOrCreateVerbose(add = false)

        if (new) {
            println("Profile does not exist!")
            return
        }

        println(profile.pathString.sha256)
    }
}