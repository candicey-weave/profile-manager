package com.gitlab.candicey.profilemanager.mode

interface IMode {
    val names: List<String>

    fun run(args: List<String>)
}