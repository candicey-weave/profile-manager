package com.gitlab.candicey.profilemanager.mode.impl

import com.gitlab.candicey.profilemanager.isLinked
import com.gitlab.candicey.profilemanager.mode.IMode
import com.gitlab.candicey.profilemanager.mode.confirm
import com.gitlab.candicey.profilemanager.mode.findTargetFile
import com.gitlab.candicey.profilemanager.profile.ProfileManager
import com.gitlab.candicey.profilemanager.toProfileOrCreateVerbose

object RemoveOption : IMode {
    override val names: List<String>
        get() = listOf("remove", "rm", "r")

    override fun run(args: List<String>) {
        val targetFile = findTargetFile(args) ?: return

        val (profile, new) = targetFile.toPath().toProfileOrCreateVerbose(add = false)
        val slicedArgs = args.drop(1)

        if (new) {
            println("Profile does not exist!")
            return
        }

        val options = slicedArgs.mapNotNull { name -> profile.options.find { it.name == name } }
        if (options.isEmpty()) {
            println("No options found!")
            return
        }

        for (option in options) {
            if (option.file isLinked targetFile) {
                println("Cannot remove option ${option.name} because it is the main file!")
                return
            }
        }

        println("Removing ${options.size} option(s) from profile ${profile.pathString}...")
        for (option in options) {
            val answer = confirm("Are you sure you want to remove ${option.name}?")
            if (!answer) {
                println("Skipping ${option.name}...")
                continue
            }

            option.file.delete()
            profile.options.remove(option)
        }

        ProfileManager.save()
    }
}