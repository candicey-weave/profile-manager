package com.gitlab.candicey.profilemanager.mode.impl

import com.gitlab.candicey.profilemanager.helper.SymbolicLinkHelper
import com.gitlab.candicey.profilemanager.isLinked
import com.gitlab.candicey.profilemanager.isSameAs
import com.gitlab.candicey.profilemanager.mode.IMode
import com.gitlab.candicey.profilemanager.mode.findTargetFile
import com.gitlab.candicey.profilemanager.profile.ProfileManager
import com.gitlab.candicey.profilemanager.toProfileOrCreateVerbose
import java.io.File

object AddProfile : IMode {
    override val names: List<String>
        get() = listOf("add", "a", "new", "n")

    override fun run(args: List<String>) {
        val targetFile = findTargetFile(args) ?: return

        val (profile, new) = targetFile.toPath().toProfileOrCreateVerbose(add = false)
        val slicedArgs = args.drop(1)

        val mainFile = File(profile.pathString)

        val options = slicedArgs
            .map(::File)
            .filter { file ->
                file.exists().also {
                    if (!it) {
                        println("File does not exist: ${file.canonicalPath}")
                    }
                }
            }
            .filter { file ->
                profile.options.none { option -> file.name == option.name }
                    .also {
                        if (!it) {
                            println("Option ${file.name} already exists!")
                        }
                    }
            }

        fun newProfile() {
            profile.createFileOptions(mainFile, *options.toTypedArray())

            profile.options.sort()

            println("Profile created: ${profile.pathString} with ${profile.options.size} option(s).")
            profile.options.forEach { println(" ${if (runCatching { it.file isLinked targetFile }.getOrElse { false }) "*" else "-"} ${it.name}") }

            ProfileManager.add(profile)

            println("Moving file(s) to profile directory...")
            for ((index, file) in listOf(mainFile, *options.toTypedArray()).withIndex()) {
                val optionFile = profile.options.find { it.name == file.name }!!
                optionFile.file.parentFile?.mkdirs()
                file.renameTo(optionFile.file)

                if (index == 0) {
                    SymbolicLinkHelper.createSymbolicLink(optionFile.file, file)
                }
            }
        }

        fun existingProfile() {
            if (options.isEmpty()) {
                println("No option to add.")
                return
            }

            val size = profile.createFileOptions(*options.toTypedArray()).size

            profile.options.sort()

            println("Profile updated: ${profile.pathString} with ${profile.options.size} option(s). $size option(s) added.")
            profile.options.forEach { println(" ${if (runCatching { it.file isLinked targetFile }.getOrElse { false }) "*" else "-"} ${it.name}") }

            println("Moving file(s) to profile directory...")
            for (file in options) {
                val optionFile = profile.options.find { it.name == file.name }!!
                optionFile.file.parentFile?.mkdirs()
                file.renameTo(optionFile.file)
            }

            ProfileManager.save()
        }

        if (new) {
            newProfile()
        } else {
            existingProfile()
        }
    }
}