plugins {
    kotlin("jvm") version ("1.9.0")

    application

    kotlin("plugin.serialization") version ("1.9.0")
}

group = "com.gitlab.candicey.profilemanager"
version = "0.1.0"

val mainClassName = "com.gitlab.candicey.profilemanager.MainKt"

repositories {
    mavenCentral()
}

dependencies {
    testImplementation(kotlin("test"))

    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.3.0")
}

tasks.test {
    useJUnitPlatform()
}

tasks.jar {
    manifest {
        attributes["Main-Class"] = mainClassName
    }

    val wantedJar = listOf<String>("stdlib", "kotlinx")
    configurations["compileClasspath"]
        .filter { wantedJar.find { wantedJarName -> it.name.contains(wantedJarName) } != null }
        .forEach { file: File ->
            from(zipTree(file.absoluteFile)) {
                duplicatesStrategy = DuplicatesStrategy.EXCLUDE
            }
        }
}

kotlin {
    jvmToolchain(17)
}

application {
    mainClass.set(mainClassName)
}